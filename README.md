# Valheim server (Kellheim) Ansible configuration

- Install git
  - `sudo apt install git`
- Install ansible
  - `sudo apt install ansible`
- Install ansible playbook(s)
  - `ansible-playbook -i hosts -K playbook-server.yml`
  - `ansible-playbook -i hosts -K playbook-tools.yml`
- Copy server shell template and edit the names/passwords/etc by hand
  - `cp start_server.sh.template templates/start_server.sh`
- Set up port forwarding so the port points to internet and start the server
  - `./start_server.sh`